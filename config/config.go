package config

import (
	"log"

	configreader "gitlab.com/msstoci/simple-api/pkg/config"
)

type MainConfig struct {
	Server struct {
		Port string
	}

	Database struct {
		SlaveDSN  string
		MasterDSN string
	}
}

func ReadConfig(cfg interface{}, module string) interface{} {
	ok := configreader.ReadModuleConfig(cfg, "etc/simple-api", module) ||
		configreader.ReadModuleConfig(cfg, "files/etc/simple-api", module) ||
		configreader.ReadModuleConfig(cfg, "../../../files/etc/simple-api", module) ||
		configreader.ReadModuleConfig(cfg, "/home/app/files/etc/simple-api", module) ||
		configreader.ReadModuleConfig(cfg, "/opt/simple-api/files/etc", module)

	if !ok {
		log.Fatalln("failed to read config for ", module)
	}
	return cfg
}
