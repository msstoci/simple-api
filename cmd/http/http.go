package http

import (
	"github.com/spf13/cobra"
	"gitlab.com/msstoci/simple-api/app/handler"
	"gitlab.com/msstoci/simple-api/config"
	"gitlab.com/msstoci/simple-api/pkg/database"
)

// function to serve server
func Run() *cobra.Command {
	return &cobra.Command{
		Use:   "http",
		Short: "use http serve",
		Long:  `Start REST API `,
		RunE: func(cmd *cobra.Command, args []string) error {
			cfg := &config.MainConfig{}
			config.ReadConfig(cfg, "main")
			db := database.New(database.Config{
				Driver: "mysql",
				Master: cfg.Database.MasterDSN,
				Slave:  []string{cfg.Database.SlaveDSN},
			})
			srv := handler.NewServer(cfg, db)
			return srv.Run()
		},
	}
}
