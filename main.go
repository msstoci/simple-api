package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/msstoci/simple-api/cmd/http"
)

func main() {
	var rootCmd = &cobra.Command{
		Use: "api-simple",
	}

	// Add Command
	rootCmd.AddCommand(http.Run())

	if err := rootCmd.Execute(); err != nil {
		panic(err)
	}
}
