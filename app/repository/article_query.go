package repository

const (
	queryCreateArticle = `
		INSERT INTO articles (userId, title, body, createdAt, updatedAt)
		VALUES (?,
				?,
				?,
				NOW(),
				NOW())
	`

	queryUpdateArticle = `
		UPDATE articles
		SET userId = ?
			, title = ?
			, body = ?
			, updatedAt = NOW() 
		WHERE id = ?
	`
)
