package repository

import (
	"context"

	"github.com/michaelhendraw/library/go/storage/database"
	"gitlab.com/msstoci/simple-api/app/object"
)

type UserRepositoryInterface interface {
	CreateUser(ctx context.Context, req object.RegistrationRequest) (userId int64, err error)
	CreateUserAccess(ctx context.Context, userId int64, accessId int64) (err error)
	CreateUserRoles(ctx context.Context, userId int64, roleId int64) (err error)
}

type userRepository struct {
	db database.Database
}

func NewUserRepository(db database.Database) *userRepository {
	return &userRepository{db: db}
}

func (ur *userRepository) CreateUser(ctx context.Context, req object.RegistrationRequest) (userId int64, err error) {
	user, err := ur.db.Exec(queryCreateUser, req.FullName, req.Email, req.Password, req.Gender)
	if err != nil {
		return
	}

	userId, err = user.LastInsertId()
	if err != nil {
		return
	}

	return
}

func (ur *userRepository) CreateUserAccess(ctx context.Context, userId int64, accessId int64) (err error) {
	_, err = ur.db.Exec(queryCreateUserAccess, userId, accessId)
	if err != nil {
		return
	}

	return
}

func (ur *userRepository) CreateUserRoles(ctx context.Context, userId int64, roleId int64) (err error) {
	_, err = ur.db.Exec(queryCreateUserRoles, userId, roleId)
	if err != nil {
		return
	}

	return
}
