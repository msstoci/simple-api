package repository

const (
	queryCreateUser = `
		INSERT INTO users (fullName, email, password, gender, createdAt, updatedAt)
		VALUES (?,
				?,
				?,
				?,
				Now(),
				Now())
	`

	queryCreateUserRoles = `
		INSERT INTO user_roles (userId, roleId) 
		VALUES (?,
			 	?)
	`

	queryCreateUserAccess = `
		INSERT INTO user_accesses (userId, accessId)
		VALUES (?,
				?)
	`
)
