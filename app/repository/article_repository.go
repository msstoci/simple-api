package repository

import (
	"context"

	"gitlab.com/msstoci/simple-api/app/object"

	"github.com/michaelhendraw/library/go/storage/database"
)

type ArticleRepositoryInterface interface {
	CreateArticle(ctx context.Context, req object.CreateArticleRequest) (err error)
	UpdateArticle(ctx context.Context, req object.UpdateArticleRequest) (err error)
}

type articleRepository struct {
	db database.Database
}

func NewArticleRepository(db database.Database) *articleRepository {
	return &articleRepository{db: db}
}

func (ar *articleRepository) CreateArticle(ctx context.Context, req object.CreateArticleRequest) (err error) {
	_, err = ar.db.Exec(queryCreateArticle, req.UserId, req.Title, req.Body)
	if err != nil {
		return
	}

	return
}

func (ar *articleRepository) UpdateArticle(ctx context.Context, req object.UpdateArticleRequest) (err error) {
	_, err = ar.db.Exec(queryUpdateArticle, req.UserId, req.Title, req.Body, req.Id)

	if err != nil {
		return
	}

	return
}
