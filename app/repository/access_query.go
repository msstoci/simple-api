package repository

const (
	queryCreateAccess = `
		INSERT INTO accesses (name) VALUES (?)
	`

	queryGetAccessIdByName = `
		SELECT 
			id 
		FROM 
			accesses 
		WHERE 
			name = ?
	`
)
