package repository

import (
	"context"
	"database/sql"

	"github.com/michaelhendraw/library/go/storage/database"
)

type RoleRepositoryInterface interface {
	CreateRole(ctx context.Context, roleName string) (roleId int64, err error)
	CreateRoleAccess(ctx context.Context, roleId int64, accessId int64) (err error)
	GetRoleAccess(ctx context.Context, userId int64, AccessName string) (isValid bool, err error)
	GetArticleIdByUserId(ctx context.Context, accessName string, userId int64, ArticleId int64) (isValid bool, err error)
	GetRoleByName(ctx context.Context, userId int64, roleName string) (isAdmin bool, err error)
}

type roleRepository struct {
	db database.Database
}

func NewRoleRepository(db database.Database) *roleRepository {
	return &roleRepository{db: db}
}

func (rr *roleRepository) CreateRole(ctx context.Context, roleName string) (roleId int64, err error) {
	roleId, err = rr.getRoleIdByName(ctx, roleName)
	if err != nil {
		return
	}

	if roleId == 0 {
		role, err := rr.db.Exec(queryCreateRole, roleName)
		if err != nil {
			return 0, err
		}

		roleId, err = role.LastInsertId()
		if err != nil {
			return 0, err
		}
	}

	return
}

func (rr *roleRepository) CreateRoleAccess(ctx context.Context, roleId int64, accessId int64) (err error) {
	role, err := rr.db.Exec(queryCreateRoleAccess, roleId, accessId)
	if err != nil {
		return
	}

	roleId, err = role.LastInsertId()
	if err != nil {
		return
	}

	return
}

func (rr *roleRepository) GetRoleAccess(ctx context.Context, userId int64, AccessName string) (isValid bool, err error) {
	var id int64

	err = rr.db.QueryRow(queryGetRoleAccessByAccessName, userId, AccessName).Scan(&id)
	if err != nil {
		if err == sql.ErrNoRows {
			isValid = false
			err = nil
		}
		return
	}

	isValid = true

	return
}

func (rr *roleRepository) GetArticleIdByUserId(ctx context.Context, accessName string, userId int64, ArticleId int64) (isValid bool, err error) {
	var id int64

	err = rr.db.QueryRow(queryGetArticleIdByUserId, accessName, userId, ArticleId).Scan(&id)
	if err != nil {
		if err == sql.ErrNoRows {
			isValid = false
			err = nil
		}
		return
	}

	isValid = true

	return
}

func (rr *roleRepository) getRoleIdByName(ctx context.Context, roleName string) (roleId int64, err error) {
	err = rr.db.QueryRow(queryGetRoleIdByName, roleName).Scan(&roleId)
	if err != nil {
		if err == sql.ErrNoRows {
			roleId = 0
			err = nil
		}
		return
	}

	return
}

func (ar *roleRepository) GetRoleByName(ctx context.Context, userId int64, roleName string) (isAdmin bool, err error) {
	var roleId int64

	err = ar.db.QueryRow(queryGetRoleByName, userId, roleName).Scan(&roleId)
	if err != nil {
		if err == sql.ErrNoRows {
			isAdmin = false
			err = nil
		}
		return
	}

	isAdmin = true

	return
}
