package repository

import (
	"context"
	"database/sql"

	"github.com/michaelhendraw/library/go/storage/database"
)

type AccessRepositoryInterface interface {
	CreateAccess(ctx context.Context, accessName string) (accessId int64, err error)
	CreateUserAccess(ctx context.Context, userId int64, accessId int64) (err error)
	GetAccessIdByName(ctx context.Context, accessName string) (accessId int64, err error)
}

type accessRepository struct {
	db database.Database
}

func NewAccessRepository(db database.Database) *accessRepository {
	return &accessRepository{db: db}
}

func (ar *accessRepository) CreateAccess(ctx context.Context, accessName string) (accessId int64, err error) {
	access, err := ar.db.Exec(queryCreateAccess, accessName)
	if err != nil {
		return 0, err
	}

	accessId, err = access.LastInsertId()
	if err != nil {
		return 0, err
	}

	return
}

func (ar *accessRepository) CreateUserAccess(ctx context.Context, userId int64, accessId int64) (err error) {
	_, err = ar.db.Exec(queryCreateUserAccess, userId, accessId)
	if err != nil {
		return
	}

	return
}

func (ar *accessRepository) GetAccessIdByName(ctx context.Context, accessName string) (accessId int64, err error) {
	err = ar.db.QueryRow(queryGetAccessIdByName, accessName).Scan(&accessId)
	if err != nil {
		if err == sql.ErrNoRows {
			accessId = 0
			err = nil
		}
		return
	}

	return
}
