package repository

const (
	queryCreateRole = `
		INSERT INTO roles (name)
		VALUES (?)
	`

	queryGetRoleIdByName = `
		SELECT id FROM roles WHERE name = ?
	`

	queryCreateRoleAccess = `
		INSERT INTO role_accesses (roleId, accessId)
		VALUES (?,
				?)
	`

	queryGetRoleAccessByAccessName = `
		SELECT u.id 
		FROM users u
		JOIN user_accesses ua ON ua.userId = u.id
		JOIN accesses a ON a.id = ua.accessId
		WHERE a.name = ? 
			AND u.id = ?
	`

	queryGetArticleIdByUserId = `
		SELECT u.id 
		FROM users u
		JOIN user_accesses ua ON ua.userId = u.id
		JOIN accesses ac ON ac.id = ua.accessId
		JOIN articles ar ON ar.userId = u.id
		WHERE ac.name = ?
			AND u.id = ?
			AND ar.id = ?
	`

	queryGetRoleByName = `
		SELECT
			r.id
		FROM
			users u
			JOIN user_roles ur ON ur.userId = u.id
			JOIN roles r ON r.id = ur.roleId 
		WHERE u.id = ?
		AND r.name = ?
	`
)
