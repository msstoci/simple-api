package helper

import (
	"encoding/json"
	"net/http"

	"github.com/labstack/gommon/log"
	"go.uber.org/zap"
)

type empty struct{}

var EmptyResponse empty

func Response(w http.ResponseWriter, httpStatusHeader int, data interface{}, errors interface{}, meta interface{}, code int) {
	apiResponse := struct {
		Data   interface{} `json:"data"`
		Errors interface{} `json:"errors"`
		Meta   interface{} `json:"meta"`
	}{
		data,
		errors,
		meta,
	}
	if code == http.StatusOK || code == http.StatusCreated || code == http.StatusNoContent {
		log.Info("API Response", zap.Any("API Response", apiResponse))
	} else {
		log.Error("API Response", zap.Any("API Response", apiResponse))
	}

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "*")
	w.Header().Set("Access-Control-Allow-Headers", "*")
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpStatusHeader)

	_ = json.NewEncoder(w).Encode(apiResponse)
}
