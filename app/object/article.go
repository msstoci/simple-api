package object

type CreateArticleRequest struct {
	UserId int64
	Title  string `json:"title"`
	Body   string `json:"body"`
}

type ArticleResponse struct {
	Title string `json:"title"`
	Body  string `json:"body"`
}

type UpdateArticleRequest struct {
	UserId int64
	Id     int64  `json:"id"`
	Title  string `json:"title"`
	Body   string `json:"body"`
}
