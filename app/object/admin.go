package object

type AddAccessRequest struct {
	UserId     int64  `json:"userId"`
	AccessName string `json:"accessName"`
}
