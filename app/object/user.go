package object

type RegistrationRequest struct {
	FullName string `json:"fullName"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Gender   string `json:"gender"`
	UserId   int64
}

type RegistrationResponse struct {
}
