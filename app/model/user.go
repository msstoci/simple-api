package model

import "time"

// User db model
type User struct {
	ID        int        `db:"id"`
	FullName  string     `db:"fullName"`
	Email     string     `db:"email"`
	Password  string     `db:"password"`
	Gender    string     `db:"gender"`
	CreatedAt time.Time  `db:"created_at"`
	UpdatedAt *time.Time `db:"updated_at"`
}
