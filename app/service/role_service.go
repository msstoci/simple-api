package service

import (
	"context"

	"gitlab.com/msstoci/simple-api/app/repository"
)

type RoleInterface interface {
	ValidateCreateArticle(ctx context.Context, userId int64) (isValid bool, err error)
	ValidateUpdateArticle(ctx context.Context, userId int64, articleId int64) (isValid bool, err error)
	IsAdministrator(ctx context.Context, userId int64) (isAdmin bool, err error)
}

type roleService struct {
	RoleRepository repository.RoleRepositoryInterface
}

func NewRoleService(
	RoleRepository repository.RoleRepositoryInterface,
) *roleService {
	return &roleService{
		RoleRepository: RoleRepository,
	}
}

func (rs *roleService) ValidateCreateArticle(ctx context.Context, userId int64) (isValid bool, err error) {
	accessName := "ArticleStore"
	isValid, err = rs.RoleRepository.GetRoleAccess(ctx, userId, accessName)
	if err != nil {
		return
	}

	return
}

func (rs *roleService) ValidateUpdateArticle(ctx context.Context, userId int64, articleId int64) (isValid bool, err error) {

	accessName := "ArticleOwnerUpdate"
	isValid1, err := rs.RoleRepository.GetArticleIdByUserId(ctx, accessName, userId, articleId)
	if err != nil {
		return
	}

	accessName = "ArticleAdminUpdate"
	isValid2, err := rs.RoleRepository.GetRoleAccess(ctx, userId, accessName)
	if err != nil {
		return
	}

	if isValid1 || isValid2 {
		isValid = true
	}

	return
}

func (rs *roleService) IsAdministrator(ctx context.Context, userId int64) (isAdmin bool, err error) {
	roleName := "SystemAdmin"
	isAdmin, err = rs.RoleRepository.GetRoleByName(ctx, userId, roleName)
	if err != nil {
		return
	}

	return
}
