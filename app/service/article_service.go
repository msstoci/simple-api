package service

import (
	"context"

	"gitlab.com/msstoci/simple-api/app/object"
	"gitlab.com/msstoci/simple-api/app/repository"
)

type ArticleInterface interface {
	CreateArticle(ctx context.Context, req object.CreateArticleRequest) (err error)
	UpdateArticle(ctx context.Context, req object.UpdateArticleRequest) (err error)
}

type articleService struct {
	ArticleRepository repository.ArticleRepositoryInterface
}

func NewArticleService(
	ArticleRepository repository.ArticleRepositoryInterface,

) *articleService {
	return &articleService{
		ArticleRepository: ArticleRepository,
	}
}

func (as *articleService) CreateArticle(ctx context.Context, req object.CreateArticleRequest) (err error) {
	err = as.ArticleRepository.CreateArticle(ctx, req)
	if err != nil {
		return
	}

	return
}

func (as *articleService) UpdateArticle(ctx context.Context, req object.UpdateArticleRequest) (err error) {
	err = as.ArticleRepository.UpdateArticle(ctx, req)
	if err != nil {
		return
	}

	return
}
