package service

import (
	"context"

	"gitlab.com/msstoci/simple-api/app/object"

	"gitlab.com/msstoci/simple-api/app/repository"
)

type AdminInterface interface {
	AddAccess(ctx context.Context, req object.AddAccessRequest) (err error)
}

type adminService struct {
	AccessRepository repository.AccessRepositoryInterface
}

func NewAdminService(
	AccessRepository repository.AccessRepositoryInterface,
) *adminService {
	return &adminService{
		AccessRepository: AccessRepository,
	}
}

func (as *adminService) AddAccess(ctx context.Context, req object.AddAccessRequest) (err error) {
	accessId, err := as.AccessRepository.GetAccessIdByName(ctx, req.AccessName)
	if err != nil {
		return
	}

	if accessId == 0 {
		accessId, err = as.AccessRepository.CreateAccess(ctx, req.AccessName)
		if err != nil {
			return
		}
	}

	err = as.AccessRepository.CreateUserAccess(ctx, req.UserId, accessId)
	if err != nil {
		return
	}

	return
}
