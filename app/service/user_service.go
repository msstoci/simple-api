package service

import (
	"context"

	"gitlab.com/msstoci/simple-api/app/object"
	"gitlab.com/msstoci/simple-api/app/repository"
)

type UserInterface interface {
	Registration(ctx context.Context, req object.RegistrationRequest) (err error)
}

type userService struct {
	UserRepository   repository.UserRepositoryInterface
	RoleRepository   repository.RoleRepositoryInterface
	AccessRepository repository.AccessRepositoryInterface
}

func NewUserService(
	UserRepository repository.UserRepositoryInterface,
	RoleRepository repository.RoleRepositoryInterface,
	AccessRepository repository.AccessRepositoryInterface,
) *userService {
	return &userService{
		UserRepository:   UserRepository,
		RoleRepository:   RoleRepository,
		AccessRepository: AccessRepository,
	}
}

func (us *userService) Registration(ctx context.Context, req object.RegistrationRequest) (err error) {
	userId, err := us.UserRepository.CreateUser(ctx, req)
	if err != nil {
		return
	}

	roleName := "Account"

	roleId, err := us.RoleRepository.CreateRole(ctx, roleName)
	if err != nil {
		return
	}

	accessId, err := us.AccessRepository.CreateAccess(ctx, req.FullName)
	if err != nil {
		return
	}

	err = us.UserRepository.CreateUserAccess(ctx, userId, accessId)
	if err != nil {
		return
	}

	err = us.RoleRepository.CreateRoleAccess(ctx, roleId, accessId)
	if err != nil {
		return
	}

	err = us.UserRepository.CreateUserRoles(ctx, userId, roleId)
	if err != nil {
		return
	}

	return
}
