package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/msstoci/simple-api/app/helper"
	"gitlab.com/msstoci/simple-api/app/object"
	"gitlab.com/msstoci/simple-api/app/service"
)

type UserHandlerInterface interface {
	UserHandler(w http.ResponseWriter, r *http.Request)
}

type userHandler struct {
	userService service.UserInterface
}

func NewUserHandler(userService service.UserInterface) *userHandler {
	return &userHandler{
		userService: userService,
	}
}

func (uh *userHandler) UserHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	switch r.Method {
	case "POST":
		uh.registration(ctx, w, r)
	default:
		fmt.Fprintf(w, "Sorry, only POST methods are supported.")
	}
}

func (uh *userHandler) registration(ctx context.Context, w http.ResponseWriter, r *http.Request) {

	// todo: userId user or administrator is hardcode
	userIdUser := 1

	decoder := json.NewDecoder(r.Body)
	var request object.RegistrationRequest
	err := decoder.Decode(&request)
	if err != nil {
		helper.Response(w, http.StatusNotAcceptable, helper.EmptyResponse, "Error Parse JSON", helper.EmptyResponse, http.StatusNotAcceptable)
		return
	}

	request.UserId = int64(userIdUser)

	err = uh.userService.Registration(ctx, request)
	if err != nil {
		helper.Response(w, http.StatusNotAcceptable, helper.EmptyResponse, err.Error(), helper.EmptyResponse, http.StatusNotAcceptable)
		return
	}

	helper.Response(w, http.StatusCreated, helper.EmptyResponse, helper.EmptyResponse, helper.EmptyResponse, http.StatusCreated)
	return
}
