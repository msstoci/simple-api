package handler

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/msstoci/simple-api/app/helper"
	"gitlab.com/msstoci/simple-api/app/object"
	"gitlab.com/msstoci/simple-api/app/service"
)

type AdminHandlerInterface interface {
	AdminHandler(w http.ResponseWriter, r *http.Request)
}

type adminHandler struct {
	adminService service.AdminInterface
	roleService  service.RoleInterface
}

func NewAdminHandler(
	adminService service.AdminInterface,
	roleService service.RoleInterface,
) *adminHandler {
	return &adminHandler{
		adminService: adminService,
		roleService:  roleService,
	}
}

func (ah *adminHandler) AdminHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	switch r.Method {
	case "POST":
		ah.addAccess(ctx, w, r)
	default:
		fmt.Fprintf(w, "Sorry, only POST methods are supported.")
	}
}

func (ah *adminHandler) addAccess(ctx context.Context, w http.ResponseWriter, r *http.Request) {

	// todo: userId user or administrator is hardcode
	userIdUser := 1

	isValid, err := ah.roleService.IsAdministrator(ctx, int64(userIdUser))
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
		}
	}

	if !isValid {
		helper.Response(w, http.StatusUnauthorized, helper.EmptyResponse, "userId access not admin", helper.EmptyResponse, http.StatusUnauthorized)
		return
	}

	decoder := json.NewDecoder(r.Body)
	var request object.AddAccessRequest
	err = decoder.Decode(&request)
	if err != nil {
		helper.Response(w, http.StatusNotAcceptable, helper.EmptyResponse, "Error Parse JSON", helper.EmptyResponse, http.StatusNotAcceptable)
		return
	}

	err = ah.adminService.AddAccess(ctx, request)
	if err != nil {
		helper.Response(w, http.StatusNotAcceptable, helper.EmptyResponse, err.Error(), helper.EmptyResponse, http.StatusNotAcceptable)
		return
	}

	helper.Response(w, http.StatusOK, helper.EmptyResponse, helper.EmptyResponse, helper.EmptyResponse, http.StatusOK)
	return
}
