package handler

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/msstoci/simple-api/app/helper"
	"gitlab.com/msstoci/simple-api/app/object"
	"gitlab.com/msstoci/simple-api/app/service"
)

type ArticleHandlerInterface interface {
	ArticleHandler(w http.ResponseWriter, r *http.Request)
}

type articleHandler struct {
	articleService service.ArticleInterface
	roleService    service.RoleInterface
}

func NewArticleHandler(
	articleService service.ArticleInterface,
	roleService service.RoleInterface,
) *articleHandler {
	return &articleHandler{
		articleService: articleService,
		roleService:    roleService,
	}
}

func (ah *articleHandler) ArticleHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	switch r.Method {
	case "POST":
		ah.createArticle(ctx, w, r)
	case "PUT":
		ah.updateArticle(ctx, w, r)
	default:
		fmt.Fprintf(w, "Sorry, only POST and PUT methods are supported.")
	}
}

func (ah *articleHandler) createArticle(ctx context.Context, w http.ResponseWriter, r *http.Request) {

	// todo: userId user or administrator is hardcode
	userIdUser := 1

	isValid, err := ah.roleService.ValidateCreateArticle(ctx, int64(userIdUser))
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
		}
	}

	if !isValid {
		helper.Response(w, http.StatusUnauthorized, helper.EmptyResponse, "account does not have ArticleStore access", helper.EmptyResponse, http.StatusUnauthorized)
		return
	}

	decoder := json.NewDecoder(r.Body)
	var request object.CreateArticleRequest
	err = decoder.Decode(&request)
	if err != nil {
		helper.Response(w, http.StatusNotAcceptable, helper.EmptyResponse, "Error Parse JSON", helper.EmptyResponse, http.StatusNotAcceptable)
		return
	}

	request.UserId = int64(userIdUser)

	err = ah.articleService.CreateArticle(ctx, request)
	if err != nil {
		helper.Response(w, http.StatusNotAcceptable, helper.EmptyResponse, err.Error(), helper.EmptyResponse, http.StatusNotAcceptable)
		return
	}

	response := object.ArticleResponse{
		Title: request.Title,
		Body:  request.Body,
	}

	helper.Response(w, http.StatusCreated, response, helper.EmptyResponse, helper.EmptyResponse, http.StatusCreated)
	return
}

func (ah *articleHandler) updateArticle(ctx context.Context, w http.ResponseWriter, r *http.Request) {

	// todo: userId user or administrator is hardcode
	userIdUser := 5

	decoder := json.NewDecoder(r.Body)
	var request object.UpdateArticleRequest
	err := decoder.Decode(&request)
	if err != nil {
		helper.Response(w, http.StatusNotAcceptable, helper.EmptyResponse, "Error Parse JSON", helper.EmptyResponse, http.StatusNotAcceptable)
		return
	}

	isValid, err := ah.roleService.ValidateUpdateArticle(ctx, int64(userIdUser), request.Id)
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
		}
	}

	if !isValid {
		helper.Response(w, http.StatusUnauthorized, helper.EmptyResponse, "account does not have ArticleStore access", helper.EmptyResponse, http.StatusUnauthorized)
		return
	}

	request.UserId = int64(userIdUser)

	err = ah.articleService.UpdateArticle(ctx, request)
	if err != nil {
		helper.Response(w, http.StatusNotAcceptable, helper.EmptyResponse, err.Error(), helper.EmptyResponse, http.StatusNotAcceptable)
		return
	}

	response := object.ArticleResponse{
		Title: request.Title,
		Body:  request.Body,
	}

	helper.Response(w, http.StatusNoContent, response, helper.EmptyResponse, helper.EmptyResponse, http.StatusNoContent)
	return
}
