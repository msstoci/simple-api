package handler

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/rs/cors"
)

func (s *Server) routes() {
	s.router.Use(cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
	}).Handler)

	s.router.Route("/simple-api/v1", func(router chi.Router) {
		router.HandleFunc("/registration", s.UserHandler.UserHandler)
		router.HandleFunc("/article", s.ArticleHandler.ArticleHandler)
		router.HandleFunc("/admin/access", s.AdminHandler.AdminHandler)

		router.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "It Works")
		})
	})
}
