package handler

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/msstoci/simple-api/config"

	"github.com/go-chi/chi"
	"github.com/michaelhendraw/library/go/storage/database"
	"github.com/rs/cors"
	"gitlab.com/msstoci/simple-api/app/repository"
	"gitlab.com/msstoci/simple-api/app/service"
	"go.uber.org/zap"
)

type Server struct {
	Cfg            *config.MainConfig
	router         *chi.Mux
	UserHandler    UserHandlerInterface
	ArticleHandler ArticleHandlerInterface
	AdminHandler   AdminHandlerInterface
}

func NewServer(cfg *config.MainConfig, db database.Database) *Server {
	router := chi.NewRouter()
	router.Use(cors.New(cors.Options{
		AllowedOrigins: nil,
	}).Handler)

	userRepository := repository.NewUserRepository(db)
	roleRepository := repository.NewRoleRepository(db)
	accessRepository := repository.NewAccessRepository(db)
	userService := service.NewUserService(userRepository, roleRepository, accessRepository)
	userHandler := NewUserHandler(userService)

	articleRepository := repository.NewArticleRepository(db)
	articleService := service.NewArticleService(articleRepository)
	roleService := service.NewRoleService(roleRepository)
	articleHandler := NewArticleHandler(articleService, roleService)

	adminService := service.NewAdminService(accessRepository)
	adminHandler := NewAdminHandler(adminService, roleService)

	return &Server{
		Cfg:            cfg,
		router:         router,
		UserHandler:    userHandler,
		ArticleHandler: articleHandler,
		AdminHandler:   adminHandler,
	}
}

func (s *Server) Run() (err error) {
	// config chi
	s.routes()

	srv := &http.Server{
		Addr:    s.Cfg.Server.Port,
		Handler: s.router,
	}
	fmt.Println("Server is running in port 8080")
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			fmt.Println("cannot start server", zap.Error(err))
		}
	}()

	c := make(chan os.Signal, 1)

	signal.Notify(c, os.Interrupt)
	<-c

	tenSecond := time.Second * 10
	ctx, cancel := context.WithTimeout(context.Background(), tenSecond)
	defer cancel()

	if err = srv.Shutdown(ctx); err != nil {
		fmt.Println("server shutdown failed", zap.Error(err))
	}

	if err == http.ErrServerClosed {
		err = nil
	}

	fmt.Println("Shutting Down")
	return nil
}
