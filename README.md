# Simple Blog API
Simple Blog API.

## Quick Start
### Technology Use
* Database MySQL

### Installation Required 
#### Install Golang 
I use go version for service compatible `go1.14.12 linux/amd64`

### Database Migration
We use database migration tools for consist migration version. Use goose for migration database tools, because very simple https://github.com/pressly/goose.

```$command
$ cd migration

// show database status and init table goose version if not exist
$ goose mysql "root:jiwamuda@/api?parseTime=true" status

// create database migration file
$ goose mysql "root:password@/database?parseTime=true" create add_column_example sql

// if success file will be generate with <version_migration>_file_name.sql

// run goose up for start db migration
$ goose mysql "root:password@/database?parseTime=true" up

// if you rollback db migration use `$ goose down`
$ goose mysql "root:password@/database?parseTime=true" down
```

### How To Run:

To run HTTP Server: `go run main.go http`

### Code Style
	"google.golang.org/appengine/log"
In golang they already define the convention style https://golang.org/doc/effective_go.html