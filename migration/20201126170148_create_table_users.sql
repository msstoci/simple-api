-- +goose Up
-- +goose StatementBegin
CREATE TABLE users (
  id INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  fullName VARCHAR(100) NOT NULL,
  email VARCHAR(100) NOT NULL UNIQUE,
  password VARCHAR(100) NOT NULL,
  gender VARCHAR(100) NOT NULL,
  createdAt datetime NOT NULL,
  updatedAt datetime NOT NULL
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE users;
-- +goose StatementEnd
