-- +goose Up
-- +goose StatementBegin
CREATE TABLE user_roles (
  userId int(10) NOT NULL,
  roleId int(10) NOT NULL
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE user_roles;
-- +goose StatementEnd
