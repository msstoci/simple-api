-- +goose Up
-- +goose StatementBegin

CREATE TABLE articles (
  id INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  userId INT(10) NOT NULL,
  title VARCHAR(255) NOT NULL,
  body VARCHAR(255) NOT NULL,
  createdAt datetime NOT NULL,
  updatedAt datetime NOT NULL
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE articles;
-- +goose StatementEnd
