-- +goose Up
-- +goose StatementBegin
CREATE TABLE user_accesses (
  userId int(10) NOT NULL,
  accessId int(10) NOT NULL
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE user_accesses;
-- +goose StatementEnd
