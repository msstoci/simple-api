-- +goose Up
-- +goose StatementBegin
CREATE TABLE role_accesses (
  roleId int(10) NOT NULL,
  accessId int(10) NOT NULL
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE role_accesses;
-- +goose StatementEnd
