-- +goose Up
-- +goose StatementBegin
CREATE TABLE accesses (
  id int(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE accesses;
-- +goose StatementEnd
