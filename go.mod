module gitlab.com/msstoci/simple-api

go 1.14

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/labstack/gommon v0.3.0
	github.com/lib/pq v1.8.0
	github.com/michaelhendraw/library v0.0.0-20191125161936-8740a0ee94df
	github.com/rs/cors v1.7.0
	github.com/spf13/cobra v1.1.1
	go.uber.org/zap v1.16.0
	gopkg.in/gcfg.v1 v1.2.3
	gopkg.in/warnings.v0 v0.1.2 // indirect
)
